from PIL import Image
import os
import shutil

#function to resize images
def resizeImg(imgData, mainFolderPath, destinationFolderPath):
	openImg = Image.open( imgData["name"] )
	width, height = openImg.size
	newImgWidth = int( imgData["width"] )
	newImgHeight = int( imgData["height"] )
	if newImgWidth > width:
		message = "Can not resize image \"" + imgData["name"] + "\". Your width value (" + str(newImgWidth) + ") is larger then image width value (" + str(width) + ")."
	elif newImgHeight > height:
		message = "Can not resize image \"" + imgData["name"] + "\". Your height value (" + str(newImgHeight) + ") is larger then image height value (" + str(height) + ")."
	else:
		resizeImg = openImg.resize( (newImgWidth, newImgHeight) )
		choesImg = imgData["name"].rsplit(".",1)
		newName = choesImg[0] + "_resize." + choesImg[1]
		resizeImg.save(newName)		
		originalImg = mainFolderPath  + "/" + newName		
		resizedImg =  destinationFolderPath + newName
		shutil.move(originalImg, resizedImg)
		message = "Image \"" + imgData["name"] + "\" resized. New image name is \"" + newName + "\""
	print message

# path to folder with script
mainFolderPath = os.path.dirname(os.path.realpath(__file__))

# list to store images form 
images = []

#create new folder to store resized images
print "Write folder name to create it. Resized images will be stored in that folder"
destinationFolder = raw_input('Folder name: ')
os.makedirs(destinationFolder);
destinationFolderPath = os.path.dirname(os.path.realpath(__file__)) + "/" + destinationFolder + "/"

# while loop for adding images' dictionaries in list
# every dictionarie has keys => name, new width and new height of img
print "Write name of image(s) to resize, \"all\" if you want to resize all image(s) in folder. When you are done, enter \"done\""
while True:
	choesImg = raw_input('Image name: ')
	if choesImg != "done" and choesImg != "all":
		newImgWidth = raw_input('New width: ')
		newImgHeight = raw_input('New height: ')
		imgData = {}
		imgData["name"] = choesImg
		imgData["width"] = int(newImgWidth)
		imgData["height"] = int(newImgHeight)
		images.append(imgData)
	elif choesImg == "all":
		newImgWidth = raw_input('New width: ')
		newImgHeight = raw_input('New height: ')
		listFiles = os.listdir(mainFolderPath)
		for file in listFiles:
			img = file.rsplit(".",1)
			if ( img[ len(img)-1 ] == "JPG" or img[ len(img)-1 ] == "JPEG" or img[ len(img)-1 ] == "jpg" or img[ len(img)-1 ] == "jpeg"):
				imgData = {}
				imgData["name"] = file
				imgData["width"] = int(newImgWidth)
				imgData["height"] = int(newImgHeight)
				images.append(imgData)
		break
	elif choesImg == "done":
		break

# call function resizeImg on every img
for image in images:
	resizeImg(image, mainFolderPath, destinationFolderPath)